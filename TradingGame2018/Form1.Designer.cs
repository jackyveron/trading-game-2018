﻿namespace TradingGame2018
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.item1 = new System.Windows.Forms.Label();
      this.item2 = new System.Windows.Forms.Label();
      this.item3 = new System.Windows.Forms.Label();
      this.item4 = new System.Windows.Forms.Label();
      this.quant1 = new System.Windows.Forms.Label();
      this.quant2 = new System.Windows.Forms.Label();
      this.quant3 = new System.Windows.Forms.Label();
      this.quant4 = new System.Windows.Forms.Label();
      this.price1 = new System.Windows.Forms.Label();
      this.price2 = new System.Windows.Forms.Label();
      this.price3 = new System.Windows.Forms.Label();
      this.price4 = new System.Windows.Forms.Label();
      this.buy1 = new System.Windows.Forms.Button();
      this.buy2 = new System.Windows.Forms.Button();
      this.buy3 = new System.Windows.Forms.Button();
      this.buy4 = new System.Windows.Forms.Button();
      this.label1 = new System.Windows.Forms.Label();
      this.money = new System.Windows.Forms.Label();
      this.locations = new System.Windows.Forms.ComboBox();
      this.sell1 = new System.Windows.Forms.Button();
      this.sell2 = new System.Windows.Forms.Button();
      this.sell3 = new System.Windows.Forms.Button();
      this.sell4 = new System.Windows.Forms.Button();
      this.SuspendLayout();
      // 
      // item1
      // 
      this.item1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.item1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.item1.Location = new System.Drawing.Point(35, 266);
      this.item1.Name = "item1";
      this.item1.Size = new System.Drawing.Size(401, 58);
      this.item1.TabIndex = 0;
      this.item1.Text = "item1";
      this.item1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // item2
      // 
      this.item2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.item2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.item2.Location = new System.Drawing.Point(35, 341);
      this.item2.Name = "item2";
      this.item2.Size = new System.Drawing.Size(401, 58);
      this.item2.TabIndex = 0;
      this.item2.Text = "item2";
      this.item2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // item3
      // 
      this.item3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.item3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.item3.Location = new System.Drawing.Point(35, 416);
      this.item3.Name = "item3";
      this.item3.Size = new System.Drawing.Size(401, 58);
      this.item3.TabIndex = 0;
      this.item3.Text = "item3";
      this.item3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // item4
      // 
      this.item4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.item4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.item4.Location = new System.Drawing.Point(35, 491);
      this.item4.Name = "item4";
      this.item4.Size = new System.Drawing.Size(401, 58);
      this.item4.TabIndex = 0;
      this.item4.Text = "item4";
      this.item4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
      // 
      // quant1
      // 
      this.quant1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.quant1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.quant1.Location = new System.Drawing.Point(456, 266);
      this.quant1.Name = "quant1";
      this.quant1.Size = new System.Drawing.Size(142, 58);
      this.quant1.TabIndex = 0;
      this.quant1.Text = "0";
      this.quant1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // quant2
      // 
      this.quant2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.quant2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.quant2.Location = new System.Drawing.Point(456, 341);
      this.quant2.Name = "quant2";
      this.quant2.Size = new System.Drawing.Size(142, 58);
      this.quant2.TabIndex = 0;
      this.quant2.Text = "0";
      this.quant2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // quant3
      // 
      this.quant3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.quant3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.quant3.Location = new System.Drawing.Point(456, 416);
      this.quant3.Name = "quant3";
      this.quant3.Size = new System.Drawing.Size(142, 58);
      this.quant3.TabIndex = 0;
      this.quant3.Text = "0";
      this.quant3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // quant4
      // 
      this.quant4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.quant4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.quant4.Location = new System.Drawing.Point(456, 491);
      this.quant4.Name = "quant4";
      this.quant4.Size = new System.Drawing.Size(142, 58);
      this.quant4.TabIndex = 0;
      this.quant4.Text = "0";
      this.quant4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // price1
      // 
      this.price1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.price1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.price1.Location = new System.Drawing.Point(620, 266);
      this.price1.Name = "price1";
      this.price1.Size = new System.Drawing.Size(142, 58);
      this.price1.TabIndex = 0;
      this.price1.Text = "0";
      this.price1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // price2
      // 
      this.price2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.price2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.price2.Location = new System.Drawing.Point(620, 341);
      this.price2.Name = "price2";
      this.price2.Size = new System.Drawing.Size(142, 58);
      this.price2.TabIndex = 0;
      this.price2.Text = "0";
      this.price2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // price3
      // 
      this.price3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.price3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.price3.Location = new System.Drawing.Point(620, 416);
      this.price3.Name = "price3";
      this.price3.Size = new System.Drawing.Size(142, 58);
      this.price3.TabIndex = 0;
      this.price3.Text = "0";
      this.price3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // price4
      // 
      this.price4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.price4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.price4.Location = new System.Drawing.Point(620, 491);
      this.price4.Name = "price4";
      this.price4.Size = new System.Drawing.Size(142, 58);
      this.price4.TabIndex = 0;
      this.price4.Text = "0";
      this.price4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // buy1
      // 
      this.buy1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buy1.Location = new System.Drawing.Point(782, 266);
      this.buy1.Name = "buy1";
      this.buy1.Size = new System.Drawing.Size(212, 58);
      this.buy1.TabIndex = 1;
      this.buy1.Tag = "0";
      this.buy1.Text = "Buy";
      this.buy1.UseVisualStyleBackColor = true;
      this.buy1.Click += new System.EventHandler(this.buy1_Click);
      // 
      // buy2
      // 
      this.buy2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buy2.Location = new System.Drawing.Point(782, 341);
      this.buy2.Name = "buy2";
      this.buy2.Size = new System.Drawing.Size(212, 58);
      this.buy2.TabIndex = 1;
      this.buy2.Tag = "1";
      this.buy2.Text = "Buy";
      this.buy2.UseVisualStyleBackColor = true;
      // 
      // buy3
      // 
      this.buy3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buy3.Location = new System.Drawing.Point(782, 416);
      this.buy3.Name = "buy3";
      this.buy3.Size = new System.Drawing.Size(212, 58);
      this.buy3.TabIndex = 1;
      this.buy3.Tag = "2";
      this.buy3.Text = "Buy";
      this.buy3.UseVisualStyleBackColor = true;
      // 
      // buy4
      // 
      this.buy4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.buy4.Location = new System.Drawing.Point(782, 491);
      this.buy4.Name = "buy4";
      this.buy4.Size = new System.Drawing.Size(212, 58);
      this.buy4.TabIndex = 1;
      this.buy4.Tag = "3";
      this.buy4.Text = "Buy";
      this.buy4.UseVisualStyleBackColor = true;
      // 
      // label1
      // 
      this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.Location = new System.Drawing.Point(456, 82);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(306, 58);
      this.label1.TabIndex = 0;
      this.label1.Text = "Money:";
      this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // money
      // 
      this.money.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
      this.money.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.money.Location = new System.Drawing.Point(784, 82);
      this.money.Name = "money";
      this.money.Size = new System.Drawing.Size(210, 58);
      this.money.TabIndex = 0;
      this.money.Text = "1000";
      this.money.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
      // 
      // locations
      // 
      this.locations.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.locations.FormattingEnabled = true;
      this.locations.Items.AddRange(new object[] {
            "Steve\'s Groceries",
            "Dwayne\'s Fruitshop",
            "Someplace With Fruit",
            "Other Fruit Place"});
      this.locations.Location = new System.Drawing.Point(35, 82);
      this.locations.Name = "locations";
      this.locations.Size = new System.Drawing.Size(401, 59);
      this.locations.TabIndex = 2;
      this.locations.SelectedIndexChanged += new System.EventHandler(this.locations_SelectedIndexChanged);
      // 
      // sell1
      // 
      this.sell1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.sell1.Location = new System.Drawing.Point(1000, 266);
      this.sell1.Name = "sell1";
      this.sell1.Size = new System.Drawing.Size(212, 58);
      this.sell1.TabIndex = 1;
      this.sell1.Tag = "0";
      this.sell1.Text = "Sell";
      this.sell1.UseVisualStyleBackColor = true;
      this.sell1.Click += new System.EventHandler(this.sell_Click);
      // 
      // sell2
      // 
      this.sell2.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.sell2.Location = new System.Drawing.Point(1000, 341);
      this.sell2.Name = "sell2";
      this.sell2.Size = new System.Drawing.Size(212, 58);
      this.sell2.TabIndex = 1;
      this.sell2.Tag = "1";
      this.sell2.Text = "Sell";
      this.sell2.UseVisualStyleBackColor = true;
      // 
      // sell3
      // 
      this.sell3.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.sell3.Location = new System.Drawing.Point(1000, 416);
      this.sell3.Name = "sell3";
      this.sell3.Size = new System.Drawing.Size(212, 58);
      this.sell3.TabIndex = 1;
      this.sell3.Tag = "2";
      this.sell3.Text = "Sell";
      this.sell3.UseVisualStyleBackColor = true;
      // 
      // sell4
      // 
      this.sell4.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.sell4.Location = new System.Drawing.Point(1000, 491);
      this.sell4.Name = "sell4";
      this.sell4.Size = new System.Drawing.Size(212, 58);
      this.sell4.TabIndex = 1;
      this.sell4.Tag = "3";
      this.sell4.Text = "Sell";
      this.sell4.UseVisualStyleBackColor = true;
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1625, 913);
      this.Controls.Add(this.locations);
      this.Controls.Add(this.sell4);
      this.Controls.Add(this.buy4);
      this.Controls.Add(this.sell3);
      this.Controls.Add(this.buy3);
      this.Controls.Add(this.sell2);
      this.Controls.Add(this.buy2);
      this.Controls.Add(this.sell1);
      this.Controls.Add(this.buy1);
      this.Controls.Add(this.price4);
      this.Controls.Add(this.quant4);
      this.Controls.Add(this.price3);
      this.Controls.Add(this.quant3);
      this.Controls.Add(this.item4);
      this.Controls.Add(this.price2);
      this.Controls.Add(this.quant2);
      this.Controls.Add(this.item3);
      this.Controls.Add(this.money);
      this.Controls.Add(this.price1);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.quant1);
      this.Controls.Add(this.item2);
      this.Controls.Add(this.item1);
      this.Name = "Form1";
      this.Text = "Form1";
      this.Load += new System.EventHandler(this.Form1_Load);
      this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label item1;
        private System.Windows.Forms.Label item2;
        private System.Windows.Forms.Label item3;
        private System.Windows.Forms.Label item4;
        private System.Windows.Forms.Label quant1;
        private System.Windows.Forms.Label quant2;
        private System.Windows.Forms.Label quant3;
        private System.Windows.Forms.Label quant4;
        private System.Windows.Forms.Label price1;
        private System.Windows.Forms.Label price2;
        private System.Windows.Forms.Label price3;
        private System.Windows.Forms.Label price4;
        private System.Windows.Forms.Button buy1;
        private System.Windows.Forms.Button buy2;
        private System.Windows.Forms.Button buy3;
        private System.Windows.Forms.Button buy4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label money;
        private System.Windows.Forms.ComboBox locations;
        private System.Windows.Forms.Button sell1;
        private System.Windows.Forms.Button sell2;
        private System.Windows.Forms.Button sell3;
        private System.Windows.Forms.Button sell4;
  }
}

